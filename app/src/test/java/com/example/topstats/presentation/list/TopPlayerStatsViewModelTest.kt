package com.example.topstats.presentation.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.topstats.data.StatsRepository
import com.example.topstats.data.model.Status
import com.example.topstats.data.model.TopPlayerStats
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

class TopPlayerStatsViewModelTest {
    private val repositoryMock = mockk<StatsRepository>()
    private val response = mockk<Response<List<TopPlayerStats>>>()
    private val topPlayerStats = mockk<TopPlayerStats>()
    private val errorMessage = "Error message"
    private val exceptionMessage = "Exception message"
    private lateinit var viewModel: TopPlayerStatsViewModel

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        viewModel = TopPlayerStatsViewModel(repositoryMock, Dispatchers.Unconfined)
    }

    @Test
    fun when_requestTopStats_IsCalledSetStatusToSuccess_IfRequestIsSuccessful() {
        every { response.isSuccessful } returns true
        every { response.body() } returns listOf(topPlayerStats)
        every {
            runBlocking {
                repositoryMock.getTopPlayerStats()
            }
        } returns response

        viewModel.requestTopStats()

        val result = viewModel.topStatsList.value!!
        assertEquals(Status.SUCCESS, result.status)
        assertEquals(listOf(topPlayerStats), result.data)
    }

    @Test
    fun when_requestTopStats_IsCalledSetStatusToError_IfRequestIsFailed() {
        every { response.isSuccessful } returns false
        every { response.errorBody() } returns ResponseBody.create(MediaType.get("text/plain"), errorMessage)
        every {
            runBlocking {
                repositoryMock.getTopPlayerStats()
            }
        } returns response

        viewModel.requestTopStats()

        val result = viewModel.topStatsList.value!!
        assertEquals(Status.ERROR, result.status)
        assertEquals("Failed to load top stats: $errorMessage", result.message)
    }

    @Test
    fun when_requestTopStats_IsCalledSetStatusToError_IfExceptionIsThrown() {
        every {
            runBlocking {
                repositoryMock.getTopPlayerStats()
            }
        } throws Exception(exceptionMessage)

        viewModel.requestTopStats()

        val result = viewModel.topStatsList.value!!
        assertEquals(Status.ERROR, result.status)
        assertEquals("Failed to load top stats: $exceptionMessage", result.message)
    }

}