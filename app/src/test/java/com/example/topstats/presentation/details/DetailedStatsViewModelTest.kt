package com.example.topstats.presentation.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.topstats.data.StatsRepository
import com.example.topstats.data.model.DetailedStats
import com.example.topstats.data.model.Status
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

class DetailedStatsViewModelTest {
    private val repositoryMock = mockk<StatsRepository>()
    private val response = mockk<Response<DetailedStats>>()
    private val detailedStats = DetailedStats(1, "position", "name", mapOf(), mapOf())
    private val teamId = 1
    private val playerId = 1
    private val errorMessage = "Error message"
    private val exceptionMessage = "Exception message"
    private lateinit var viewModel: DetailedStatsViewModel

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        viewModel = DetailedStatsViewModel(repositoryMock, Dispatchers.Unconfined)
    }

    @Test
    fun when_requestDetailedStats_IsCalledSetStatusToSuccess_IfRequestIsSuccessful() {
        every { response.isSuccessful } returns true
        every { response.body() } returns detailedStats
        every {
            runBlocking {
                repositoryMock.getPlayerDetailedStats(teamId, playerId)
            }
        } returns response

        viewModel.requestDetailedStats(teamId, playerId)

        val result = viewModel.detailedStats.value!!
        assertEquals(Status.SUCCESS, result.status)
        assertEquals(detailedStats, result.data)
    }

    @Test
    fun when_requestDetailedStats_IsCalledSetStatusToError_IfRequestIsFailed() {
        every { response.isSuccessful } returns false
        every { response.errorBody() } returns ResponseBody.create(MediaType.get("text/plain"), errorMessage)
        every {
            runBlocking {
                repositoryMock.getPlayerDetailedStats(teamId, playerId)
            }
        } returns response

        viewModel.requestDetailedStats(teamId, playerId)

        val result = viewModel.detailedStats.value!!
        assertEquals(Status.ERROR, result.status)
        assertEquals("Failed to load detailed stats: $errorMessage", result.message)
    }

    @Test
    fun when_requestDetailedStats_IsCalledSetStatusToError_IfExceptionIsThrown() {
        every {
            runBlocking {
                repositoryMock.getPlayerDetailedStats(teamId, playerId)
            }
        } throws Exception(exceptionMessage)

        viewModel.requestDetailedStats(teamId, playerId)

        val result = viewModel.detailedStats.value!!
        assertEquals(Status.ERROR, result.status)
        assertEquals("Failed to load detailed stats: $exceptionMessage", result.message)
    }
}