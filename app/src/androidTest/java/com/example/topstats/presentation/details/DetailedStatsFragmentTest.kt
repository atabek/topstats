package com.example.topstats.presentation.details

import android.content.Context
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.example.topstats.App
import com.example.topstats.R
import com.example.topstats.config.FileHelper
import com.example.topstats.config.MockWebServerRule
import com.example.topstats.presentation.MainActivity
import okhttp3.mockwebserver.MockResponse
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailedStatsFragmentTest {
    private val context: Context = InstrumentationRegistry.getInstrumentation().context
    private val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, true, false)

    @get:Rule
    val mockWebServerRule = MockWebServerRule()

    @Before
    fun setup() {
        app.baseUrl = mockWebServerRule.server.url("/").toString()
    }

    @Test
    fun whenPlayerIsClicked_openPlayerDetails() {
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "top_stats.json")))
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "detailed_stats.json")))

        activityRule.launchActivity(null)

        // Click first player in A team
        onView(withContentDescription("Profile image of P. Wallace")).perform(click())

        // Verify player details are correct
        onView(withText("Detailed Stats")).check(matches(isDisplayed()))
        onView(withText("Peter Wallace (Hooker)")).check(matches(isDisplayed()))

        // Verify that first tab shows empty state as we don't have any data to show
        onView(withText("Last match stats")).perform(click())
        onView(allOf(withId(R.id.tabsEmpty), isDisplayed())).check(matches(isDisplayed()))

        // Switch to second tab, and verify that it has stats to show
        onView(withText("Season stats")).perform(click())
        onView(allOf(withText("games"), hasSibling(withText("19"))))
        onView(allOf(withText("tackles"), hasSibling(withText("695"))))
        onView(allOf(withText("errors"), hasSibling(withText("12"))))
    }

    @Test
    fun whenPlayerIsClicked_butNetworkRequestFailed_showEmptyState() {
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "top_stats.json")))
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(400).setBody("{}"))
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "detailed_stats.json")))

        activityRule.launchActivity(null)

        // Click first player in A team
        onView(withContentDescription("Profile image of P. Wallace")).perform(click())

        // As request returned status code 400, we show empty state
        onView(withId(R.id.detailedStatsRetry)).check(matches(isDisplayed()))
        onView(withId(R.id.detailedStatsEmptyText)).check(matches(isDisplayed()))

        // Click retry button
        onView(withId(R.id.detailedStatsRetry)).perform(click())

        // Verify player details are correct
        onView(withText("Detailed Stats")).check(matches(isDisplayed()))
        onView(withText("Peter Wallace (Hooker)")).check(matches(isDisplayed()))
    }
}