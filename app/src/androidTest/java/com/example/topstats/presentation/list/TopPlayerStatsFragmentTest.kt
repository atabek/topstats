package com.example.topstats.presentation.list

import android.content.Context
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.example.topstats.App
import com.example.topstats.R
import com.example.topstats.config.FileHelper
import com.example.topstats.config.MockWebServerRule
import com.example.topstats.presentation.MainActivity
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TopPlayerStatsFragmentTest {
    private val context: Context = InstrumentationRegistry.getInstrumentation().context
    private val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, true, false)

    @get:Rule
    val mockWebServerRule = MockWebServerRule()

    @Before
    fun setup() {
        app.baseUrl = mockWebServerRule.server.url("/").toString()
    }

    @Test
    fun whenDataIsAvailable_firstPlayerIsVisible() {
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "top_stats.json")))

        activityRule.launchActivity(null)

        onView(withText("Top Player Stats")).check(matches(isDisplayed()))
        onView(withId(R.id.topStatType)).check(matches(withText("tackles")))
        onView(withId(R.id.playerStatsView)).apply {
            check(matches(hasDescendant(withText("P. Wallace"))))
            check(matches(hasDescendant(withText("51"))))
            check(matches(hasDescendant(withText("Hooker, Jumper#9"))))
        }
    }

    @Test
    fun whenDataIsNotAvailable_emptyStateIsVisible_clickRetryWillSendRequestOnceMore() {
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(400).setBody("{}"))
        mockWebServerRule.server.enqueue(MockResponse().setResponseCode(200).setBody(FileHelper.getStringFromFile(context, "top_stats.json")))

        activityRule.launchActivity(null)

        onView(withText("Top Player Stats")).check(matches(isDisplayed()))
        // As request returned status code 400, we show empty state
        onView(withId(R.id.topStatsRetry)).check(matches(isDisplayed()))
        onView(withId(R.id.topStatsEmptyText)).check(matches(isDisplayed()))

        // Click retry button
        onView(withId(R.id.topStatsRetry)).perform(click())

        // Data is visible
        onView(withId(R.id.topStatType)).check(matches(withText("tackles")))
        onView(withId(R.id.playerStatsView)).apply {
            check(matches(hasDescendant(withText("A. Tolman"))))
            check(matches(hasDescendant(withText("54"))))
            check(matches(hasDescendant(withText("Prop Forward, Jumper#8"))))
        }
    }

}