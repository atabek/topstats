package com.example.topstats.config

import android.content.Context


object FileHelper {

    @Throws(Exception::class)
    fun getStringFromFile(context: Context, filePath: String): String {
        return context.resources.assets.open(filePath).bufferedReader().use {
            it.readText()
        }
    }

}