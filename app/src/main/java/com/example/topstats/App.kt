package com.example.topstats

import android.app.Application
import com.example.topstats.di.Injections
import org.koin.android.ext.android.startKoin

class App : Application() {
    var baseUrl = "https://statsapi.foxsports.com.au/"

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(Injections.dataModule, Injections.viewModelModule))
    }
}