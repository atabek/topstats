package com.example.topstats.data.model

import com.google.gson.annotations.SerializedName

data class TopPlayer(
    @SerializedName("id") val id: Int,
    @SerializedName("position") val position: String,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("short_name") val shortName: String,
    @SerializedName("stat_value") val statValue: Int,
    @SerializedName("jumper_number") val jumperNumber: Int
)

data class Team(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("code") val code: String,
    @SerializedName("short_name") val shortName: String,
    @SerializedName("top_players") val topPlayers: List<TopPlayer>
)

data class TopPlayerStats(
    @SerializedName("match_id") val matchId: String,
    @SerializedName("team_A") val teamA: Team,
    @SerializedName("team_B") val teamB: Team,
    @SerializedName("stat_type") val statType: String
)