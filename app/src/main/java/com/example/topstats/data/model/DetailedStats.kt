package com.example.topstats.data.model

import com.google.gson.annotations.SerializedName

data class DetailedStats(
    @SerializedName("id") val id: Int,
    @SerializedName("position") val position: String,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("last_match_stats") val lastMatchStats: Map<String, String>,
    @SerializedName("series_season_stats") val seriesSeasonStats: Map<String, String>
)