package com.example.topstats.data.network

import com.example.topstats.data.model.DetailedStats
import com.example.topstats.data.model.TopPlayerStats
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("3.0/api/sports/league/matches/NRL20172101/topplayerstats.json;type=fantasy_points;type=tackles;type=runs;type=run_metres?limit=5&userkey=A00239D3-45F6-4A0A-810C-54A347F144C2")
    fun getTopPlayerStats(): Deferred<Response<List<TopPlayerStats>>>

    @GET("3.0/api/sports/league/series/1/seasons/115/teams/{team_id}/players/{player_id}/detailedstats.json?&userkey=9024ec15-d791-4bfd-aa3b-5bcf5d36da4f")
    fun getPlayerDetailedStats(@Path("team_id") teamId: Int, @Path("player_id") playerId: Int): Deferred<Response<DetailedStats>>
}