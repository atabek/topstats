package com.example.topstats.data

import com.example.topstats.data.model.DetailedStats
import com.example.topstats.data.model.TopPlayerStats
import com.example.topstats.data.network.ApiService
import retrofit2.Response

class StatsRepository(private val apiService: ApiService) {

    suspend fun getTopPlayerStats(): Response<List<TopPlayerStats>> {
        return apiService.getTopPlayerStats().await()
    }

    suspend fun getPlayerDetailedStats(teamId: Int, playerId: Int): Response<DetailedStats> {
        return apiService.getPlayerDetailedStats(teamId, playerId).await()
    }

}