package com.example.topstats.presentation.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.topstats.R
import kotlinx.android.synthetic.main.item_detailed_stats.view.*

class TabsAdapter(private val data: Map<String, String>) : RecyclerView.Adapter<TabsAdapter.ViewHolder>() {
    private val keys = data.keys.toList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_detailed_stats, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val key = keys[position]
        holder.bind(key, data.getValue(key))
    }

    override fun getItemCount() = data.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(key: String, value: String) {
            itemView.itemDetailedKey.text = key
            itemView.itemDetailedValue.text = value
        }
    }
}