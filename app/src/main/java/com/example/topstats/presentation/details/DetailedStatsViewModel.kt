package com.example.topstats.presentation.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.topstats.data.StatsRepository
import com.example.topstats.data.model.DetailedStats
import com.example.topstats.data.model.Resource
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class DetailedStatsViewModel(
    private val repository: StatsRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel(), CoroutineScope {
    val detailedStats = MutableLiveData<Resource<DetailedStats>>()

    private val viewModelJob = Job()

    override fun onCleared() {
        viewModelJob.cancel()
        super.onCleared()
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + viewModelJob

    fun requestDetailedStats(teamId: Int, playerId: Int) {
        detailedStats.postValue(Resource.loading())
        launch(dispatcher) {
            try {
                val result = repository.getPlayerDetailedStats(teamId, playerId)

                detailedStats.postValue(
                    when (result.isSuccessful) {
                        true -> Resource.success(result.body())
                        false -> Resource.error("Failed to load detailed stats: ${result.errorBody()?.string()}")
                    }
                )
            } catch (e: Exception) {
                detailedStats.postValue(Resource.error("Failed to load detailed stats: ${e.localizedMessage}"))
            }
        }
    }
}