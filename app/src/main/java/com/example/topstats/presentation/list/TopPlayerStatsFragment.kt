package com.example.topstats.presentation.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.topstats.R
import com.example.topstats.data.model.Status
import kotlinx.android.synthetic.main.fragment_top_stats.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopPlayerStatsFragment : Fragment() {
    private val viewModel: TopPlayerStatsViewModel by viewModel()
    private val adapter = TopPlayerStatsAdapter()

    companion object {
        const val TAG = "TopPlayerStatsFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.requestTopStats()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_top_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        topStatsRecycler.adapter = adapter

        topStatsRetry.setOnClickListener {
            viewModel.requestTopStats()
        }

        viewModel.topStatsList.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    topStatsEmptyState.visibility = View.GONE
                    topStatsProgress.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    topStatsEmptyState.visibility = View.GONE
                    topStatsProgress.visibility = View.GONE
                    adapter.submitList(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, it.message)
                    topStatsEmptyState.visibility = View.VISIBLE
                    topStatsProgress.visibility = View.GONE
                }
            }
        })
    }
}