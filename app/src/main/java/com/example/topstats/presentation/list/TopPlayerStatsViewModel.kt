package com.example.topstats.presentation.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.topstats.data.StatsRepository
import com.example.topstats.data.model.Resource
import com.example.topstats.data.model.TopPlayerStats
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class TopPlayerStatsViewModel(
    private val repository: StatsRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel(), CoroutineScope {
    val topStatsList = MutableLiveData<Resource<List<TopPlayerStats>>>()

    private val viewModelJob = Job()

    override fun onCleared() {
        viewModelJob.cancel()
        super.onCleared()
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + viewModelJob

    fun requestTopStats() {
        topStatsList.postValue(Resource.loading())
        launch(dispatcher) {
            try {
                val result = repository.getTopPlayerStats()

                topStatsList.postValue(
                    when (result.isSuccessful) {
                        true -> Resource.success(result.body())
                        false -> Resource.error("Failed to load top stats: ${result.errorBody()?.string()}")
                    }
                )
            } catch (e : Exception) {
                topStatsList.postValue(Resource.error("Failed to load top stats: ${e.localizedMessage}"))
            }
        }
    }
}