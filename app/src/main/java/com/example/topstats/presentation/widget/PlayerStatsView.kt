package com.example.topstats.presentation.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.example.topstats.R
import com.example.topstats.data.model.TopPlayerStats
import com.example.topstats.presentation.setImage
import kotlinx.android.synthetic.main.item_player_stats.view.*

class PlayerStatsView : LinearLayout {

    constructor(context: Context?) : super(context) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        orientation = LinearLayout.VERTICAL
    }

    fun setup(topPlayerStats: TopPlayerStats, callback: (Int, Int) -> View.OnClickListener) {
        val count = Math.max(topPlayerStats.teamA.topPlayers.size, topPlayerStats.teamB.topPlayers.size)
        val inflater = LayoutInflater.from(context)

        for (i in 0 until count) {
            val view = inflater.inflate(R.layout.item_player_stats, this, false)
            val playerA = topPlayerStats.teamA.topPlayers[i]
            val playerB = topPlayerStats.teamB.topPlayers[i]

            view.teamAPlayerName.text = playerA.shortName
            view.teamAPlayerRanking.text = playerA.statValue.toString()
            view.teamAPlayerImage.setImage(playerA.id)
            view.teamAPlayerImage.contentDescription = "Profile image of ${playerA.shortName}"
            view.teamAPlayerImage.setOnClickListener(callback(topPlayerStats.teamA.id, playerA.id))
            view.teamAPlayerPosition.text = resources.getString(R.string.position_jumper, playerA.position, playerA.jumperNumber)

            view.teamBPlayerName.text = playerB.shortName
            view.teamBPlayerRanking.text = playerB.statValue.toString()
            view.teamBPlayerImage.setImage(playerB.id)
            view.teamBPlayerImage.contentDescription = "Profile image of ${playerB.shortName}"
            view.teamBPlayerImage.setOnClickListener(callback(topPlayerStats.teamB.id, playerB.id))
            view.teamBPlayerPosition.text = resources.getString(R.string.position_jumper, playerB.position, playerB.jumperNumber)
            addView(view)
        }
    }

}