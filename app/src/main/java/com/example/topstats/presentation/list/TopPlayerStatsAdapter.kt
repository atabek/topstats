package com.example.topstats.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.topstats.R
import com.example.topstats.data.model.TopPlayerStats
import com.example.topstats.presentation.details.DetailedStatsFragment
import kotlinx.android.synthetic.main.item_top_stats.view.*

class TopPlayerStatsAdapter : ListAdapter<TopPlayerStats, TopPlayerStatsAdapter.ViewHolder>(TopStatsDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_top_stats, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(stats: TopPlayerStats) {
            itemView.topStatType.text = stats.statType.replace("_", " ")
            itemView.playerStatsView.setup(stats) { teamId: Int, playerId: Int ->
                View.OnClickListener {
                    val bundle = Bundle()
                    bundle.putInt(DetailedStatsFragment.TEAM_ID, teamId)
                    bundle.putInt(DetailedStatsFragment.PLAYER_ID, playerId)
                    it.findNavController().navigate(R.id.actionPlayerDetails, bundle)
                }
            }
        }
    }
}

private class TopStatsDiffCallback : DiffUtil.ItemCallback<TopPlayerStats>() {
    override fun areItemsTheSame(oldItem: TopPlayerStats, newItem: TopPlayerStats) =
        (oldItem.matchId == newItem.matchId && oldItem.statType == newItem.statType)

    override fun areContentsTheSame(oldItem: TopPlayerStats, newItem: TopPlayerStats) = oldItem == newItem
}