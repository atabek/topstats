package com.example.topstats.presentation.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.topstats.R
import com.example.topstats.data.model.Status
import com.example.topstats.presentation.setImage
import kotlinx.android.synthetic.main.fragment_detailed_stats.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailedStatsFragment : Fragment() {
    private val viewModel: DetailedStatsViewModel by viewModel()
    private var teamId = 0
    private var playerId = 0

    companion object {
        private const val TAG = "DetailedStatsFragment"
        const val TEAM_ID = "team_id"
        const val PLAYER_ID = "player_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        teamId = arguments?.getInt(TEAM_ID, 0) ?: 0
        playerId = arguments?.getInt(PLAYER_ID, 0) ?: 0

        viewModel.requestDetailedStats(teamId, playerId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detailed_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailedStatsRetry.setOnClickListener {
            viewModel.requestDetailedStats(teamId, playerId)
        }

        viewModel.detailedStats.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    detailedStatsEmptyState.visibility = View.GONE
                    detailedPlayerStats.visibility = View.GONE
                    detailedStatsProgress.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    detailedStatsEmptyState.visibility = View.GONE
                    detailedStatsProgress.visibility = View.GONE

                    detailedPlayerStats.visibility = View.VISIBLE
                    detailedPlayerPhoto.setImage(it.data?.id ?: 0)
                    detailedPlayerName.text = "${it.data?.fullName} (${it.data?.position})"

                    detailedPlayerStats.adapter = DetailedStatsPagerAdapter(requireContext(), it.data?.lastMatchStats ?: mapOf(), it.data?.seriesSeasonStats ?: mapOf())
                    detailedPlayerStatsTabs.setupWithViewPager(detailedPlayerStats)
                }
                Status.ERROR -> {
                    Log.e(TAG, it.message)
                    detailedStatsEmptyState.visibility = View.VISIBLE
                    detailedPlayerStats.visibility = View.GONE
                    detailedStatsProgress.visibility = View.GONE
                }
            }
        })
    }
}