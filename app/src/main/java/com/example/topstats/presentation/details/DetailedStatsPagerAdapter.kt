package com.example.topstats.presentation.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.topstats.R
import kotlinx.android.synthetic.main.tab_stats.view.*

class DetailedStatsPagerAdapter(
    context: Context,
    private val lastMatchStats: Map<String, String>,
    private val seriesSeasonStats: Map<String, String>
) : PagerAdapter() {
    private val tabs = context.resources.getStringArray(R.array.detailed_stats_tabs)
    private val layoutInflater = LayoutInflater.from(context)

    companion object {
        private const val TAB_COUNT = 2
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater.inflate(R.layout.tab_stats, container, false)
        val data = getData(position)
        if (data.isEmpty()) {
            view.tabsEmpty.visibility = View.VISIBLE
        } else {
            view.tabsEmpty.visibility = View.GONE
            view.tabsRecycler.adapter = TabsAdapter(data)
        }
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any)= view == obj

    override fun getCount() = TAB_COUNT

    override fun getPageTitle(position: Int): CharSequence? {
        return tabs[position]
    }

    private fun getData(position: Int): Map<String, String> {
        val data: Map<String, String> = HashMap()

        return when(position) {
            0 -> lastMatchStats
            1 -> seriesSeasonStats
            else -> data
        }
    }
}