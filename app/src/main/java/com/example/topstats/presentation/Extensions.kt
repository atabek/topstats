package com.example.topstats.presentation

import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

private const val BLANK_IMAGE = "http://media.foxsports.com.au/match-centre/includes/images/headshots/headshot-blank-large.jpg"

fun ImageView.setImage(playerId: Int) {
    val imageUrl = "http://media.foxsports.com.au/match-centre/includes/images/headshots/nrl/$playerId.jpg"
    Picasso.get()
        .load(imageUrl)
        .into(this, object : Callback {
            override fun onSuccess() {}

            override fun onError(e: Exception?) {
                Picasso.get()
                    .load(BLANK_IMAGE)
                    .into(this@setImage)
            }
        })
}