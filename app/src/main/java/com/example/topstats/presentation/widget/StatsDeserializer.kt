package com.example.topstats.presentation.widget

import com.example.topstats.data.model.DetailedStats
import com.google.gson.*
import java.lang.reflect.Type



class StatsDeserializer : JsonDeserializer<DetailedStats> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DetailedStats {
        val jsonObject = json?.asJsonObject

        val id = jsonObject?.get("id")?.asInt ?: 0
        val position = jsonObject?.get("position")?.asString ?: ""
        val fullName = jsonObject?.get("full_name")?.asString ?: ""
        val lastMatchMap: Map<String, String> = getMap(jsonObject?.get("last_match_stats") as JsonObject)
        val seasonMap: Map<String, String> = getMap(jsonObject.get("series_season_stats") as JsonObject)

        return DetailedStats(id, position, fullName, lastMatchMap, seasonMap)
    }

    private fun getMap(jsonObject: JsonObject): Map<String, String> {
        val map: MutableMap<String, String> = HashMap()

        for ((k, v) in jsonObject.entrySet()) {
            if (v !is JsonNull) {
                map[k.replace("_", " ")] = v.asString
            }
        }

        return map
    }

}