package com.example.topstats.di

import com.example.topstats.App
import com.example.topstats.data.StatsRepository
import com.example.topstats.data.model.DetailedStats
import com.example.topstats.data.network.ApiService
import com.example.topstats.presentation.details.DetailedStatsViewModel
import com.example.topstats.presentation.list.TopPlayerStatsViewModel
import com.example.topstats.presentation.widget.StatsDeserializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Injections {

    val dataModule = module {
        single<Gson> {
            GsonBuilder()
                .registerTypeAdapter(DetailedStats::class.java, StatsDeserializer())
                .create()
        }
        single<Converter.Factory> { GsonConverterFactory.create(get()) }
        single<CallAdapter.Factory> { CoroutineCallAdapterFactory() }
        single {
            Retrofit.Builder()
                .baseUrl((androidApplication() as App).baseUrl)
                .addConverterFactory(get())
                .addCallAdapterFactory(get())
                .build()
        }
        single { get<Retrofit>().create(ApiService::class.java) }
        single { StatsRepository(get()) }
    }

    val viewModelModule = module {
        viewModel { TopPlayerStatsViewModel(get()) }
        viewModel { DetailedStatsViewModel(get()) }
    }

}