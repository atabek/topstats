# Top Stats #

This is sample android application to show some stats about NRL players, built using Kotlin, Architecture Components 
(LiveData, ViewModel, Navigation), Koin as DI, Retrofit, Picasso, coroutines, unit and UI testing.

## Architecture ##

MVVM is used as a presentation pattern, where ViewModels have LiveData, to which Fragments subscribe, and Repository is 
used as a place to get data from external sources (via Retrofit).

## Unit Tests ##

Unit test covered ViewModels, i.e. what value it needs to push to LiveData objects depending on network request.

## UI Tests ##

Espresso tests cover basic scenarios like when we have proper data and when we don't have any data (due to some network 
failures). Network requests are mocked via MockWebServer. 